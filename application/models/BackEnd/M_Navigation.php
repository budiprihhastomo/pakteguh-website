<?php defined('BASEPATH') OR exit('Dilarang Mengakses File ini !');

class M_Navigation extends CI_Model
{
    private $T_NAV = 'my_navigation';

    public function get_main_menu()
    {
        return $this->db->get_where($this->T_NAV, ['parent_id'=>0])->result();
    }

    public function get_sub_menu($id)
    {
        return $this->db->get_where($this->T_NAV, ['parent_id'=>$id]);
    }
}
