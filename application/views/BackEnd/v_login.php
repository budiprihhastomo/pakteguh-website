<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>Login - Pak Teguh Website</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
    <!-- External CSS libraries -->
    <link type="text/css" rel="stylesheet" href="../assets/css/bootstrap.min.css">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800%7CPoppins:400,500,700,800,900%7CRoboto:100,300,400,400i,500,700">

    <!-- Custom Stylesheet -->
    <link type="text/css" rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" type="text/css" id="style_sheet" href="../assets/css/default.css">

</head>
<body id="top">
<div class="page_loader"></div>
<div class="login-5">
    <div class="container">
        <div class="row login-box">
            <div class="col-lg-5 col-md-12 col-pad-0 bg-img none-992">
                <a href="#">
                    <img src="<?=base_url('assets/img/logo.svg')?>" class="logo" alt="logo">
                </a>
                <h3>Selamat Datang,</h3>
                <p>Untuk berpartisipasi dalam pengembangan konten website ini. Daftar atau login sekarang!</p>
                <!-- <a href="register-5.html" class="btn-outline">Daftar</a> -->
                <ul class="social-list clearfix">
                    <li><a href="#" class="facebook-bg"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="<?=$btn_google?>" class="google-bg"><i class="fa fa-google-plus"></i></a></li>
                </ul>
            </div>
            <div class="col-lg-7 col-md-12 col-pad-0 align-self-center">
                <div class="login-inner-form">
                    <div class="details">
                        <h3>Ayo Masuk Sekarang!</h3>
                        <form action="#" method="POST">
                            <div class="form-group">
                                <input type="email" name="email" class="input-text" placeholder="Alamat Email">
                            </div>
                            <div class="form-group">
                                <input type="password" name="Password" class="input-text" placeholder="Kata Sandi">
                            </div>
                            <div class="checkbox clearfix">
                                <div class="form-check checkbox-theme">
                                    <input class="form-check-input" type="checkbox" value="" id="rememberMe">
                                    <label class="form-check-label" for="rememberMe">Ingat Saya!</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn-md btn-theme">Login</button>
                            </div>
                        </form>
                        <p>Masuk Dengan Akun<br><span> Google / Facebook</span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- External JS libraries -->
<script src="../assets/js/core/jquery.3.2.1.min.js"></script>
<script src="../assets/js/core/popper.min.js"></script>
<script src="../assets/js/core/bootstrap.min.js"></script>
<!-- Custom JS Script -->
</body>
</html>