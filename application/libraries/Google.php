<?php 
require_once('vendor/autoload.php');

class Google {
	protected $CI;

	public function __construct()
	{
		$this->CI =& get_instance();
    $this->CI->load->library('session');
		$this->CI->config->load('google');
		$this->config_google();
	}

	private function config_google()
	{
		$this->client = new Google_Client();
		$this->client->setClientId($this->CI->config->item('google_client_id'));
		$this->client->setClientSecret($this->CI->config->item('google_client_secret'));
		$this->client->setRedirectUri($this->CI->config->item('google_redirect_url'));
		$this->client->setScopes(array(
			"https://www.googleapis.com/auth/plus.login",
			"https://www.googleapis.com/auth/plus.me",
			"https://www.googleapis.com/auth/userinfo.email",
			"https://www.googleapis.com/auth/userinfo.profile"
			)
		);
	}

	public function get_login_url()
	{
		return  $this->client->createAuthUrl();
	}

	public function validate(){		
		if (isset($_GET['code'])) 
		{
		  $this->client->authenticate($_GET['code']);
		  $_SESSION['access_token'] = $this->client->getAccessToken();
		}

		if (isset($_SESSION['access_token']) && $_SESSION['access_token']) 
		{
		  $this->client->setAccessToken($_SESSION['access_token']);
		  $plus = new Google_Service_Plus($this->client);
			$person = $plus->people->get('me');
			$info = 
			[
				'id'					=> $person['id'],
				'email'				=> $person['emails'][0]['value'],
				'full_name'		=> $person['displayName'],
				'f_name'			=> $person['name']['givenName'],
				'l_name'			=> $person['name']['familyName'],
				'profile_pic' => $person['image']['url'].'?sz=1000'
			];
			return  $info;
		}
	}
}