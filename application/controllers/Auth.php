<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Auth extends CI_Controller {
	function __construct() 
	{
		parent::__construct();
        $this->load->library('google');
    }

	public function index()
	{
		$data['btn_google'] = $this->google->get_login_url();
		$this->load->view('BackEnd/v_login',$data);
	}

	public function oauth2callback()
	{
		$google_data = $this->google->validate();
		$session_data = array(
			'id'				=> $google_data['id'],
			'full_name'			=> $google_data['full_name'],
			'f_name'			=> $google_data['f_name'],
			'l_name'			=> $google_data['l_name'],
			'email'				=> $google_data['email'],
			'source'			=> 'Google',
			'profile_pic'		=> $google_data['profile_pic'],
			'link'				=> $google_data['link'],
			'sess_logged_in'	=> 1
		);
		$this->session->set_userdata($session_data);
		redirect(base_url('BackEnd/Dashboard'));
	}
	public function logout()
	{
		session_destroy();
		unset($_SESSION['access_token']);
		$session_data['sess_logged_in'] = 0;
		$this->session->set_userdata($session_data);
		redirect(base_url());
	}
}
