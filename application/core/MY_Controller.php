<?php defined('BASEPATH') OR exit('Dilarang Mengakses File ini !');

class MY_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function renderView($data = [])
    {
        $data['menu'] = $this->NAV->get_main_menu();
        $this->load->view('BackEnd/structured_view',$data);
    }

    public function show_404_custom()
    {
        return $this->var['_content'] = 'BackEnd/error';
    }

    public function is_authenticated()
    {
        if($this->session->userdata('sess_logged_in') != 1)
        {
            redirect(base_url('auth'),'refresh');
        }
    }
}
